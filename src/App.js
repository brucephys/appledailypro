import React from 'react';
import './App.css';
import _ from 'lodash'
import { Table } from 'semantic-ui-react'
import { createStore } from 'redux';

const initialState = {
  column: null,
  data: [
    { runN: 'H-010720-A', key: "HIL: HATCN 100â„« | HTL: HHT44 400â„« | EBL: HTM023 50â„« | EML: HM203:GD785 12% 400â„« | ETL: Liq:NTU66 35% 350â„« | EIL: Liq 10â„« | Cath: Al 1000â„«"},
    { runN: 'H-010720-B', key: "HIL: HATCN 100â„« | HTL: HHT44 400â„« | EBL: HTM023 50â„« | EML: HM203:GD582 12% 400â„« | ETL: Liq:NTU66 35% 350â„« | EIL: Liq 10â„« | Cath: Al 1000â„«"},
  ],
  direction: null,
  filterText: '',

};

const store = createStore(reducer, initialState);

function reducer(state, action) {
  switch (action.type) {
    case 'CHANGE_SORT':{
      console.log('fire')
      console.log(state)
      console.log(action)
      if (state.column === action.column) {
        return {
          ...state,
          data: state.data.reverse(),
          direction:
            state.direction === 'ascending' ? 'descending' : 'ascending',
        }
      }
      return {
        column: action.column,
        data: _.sortBy(state.data, [action.column]),
        direction: 'ascending',
        filterText: state.filterText
      }
    }
    case 'Filter_MESSAGE':{
      const string = action.text

      return {
        ...state, filterText: string
      }
    }
    case 'ADD_MESSAGE':{
      console.log('hihi')
      console.log(state)
      console.log({
        ...state, data: action.state
      })
      return {
        ...state, data: action.state
      }
    }
    default:{
      return state;
    }
  }
}


const TableExampleSortable = (tableData) => {
  console.log('table1')
  console.log(tableData)
  // const searchString = tableData.tableData.filterText
  // const data = tableData.tableData.data
  const {column, data, direction, filterText} = tableData.tableData
  
  // const [state, dispatch] = React.useReducer(reducer, {
  //   column: null,
  //   data: sdata,
  //   direction: null,
  //   filterText: searchString
  // })

  // const { column, data, direction,  } = state

  let newData = []
  if (filterText){
    newData = data.filter( (d) => {
      return (d.runN.toLowerCase().includes(filterText.toLowerCase()) ||
        d.key.toLowerCase().includes(filterText.toLowerCase())
      )
    })
  } else{
    newData = data
  }
  
  console.log(data)
  console.log(newData)

  return (
    <Table sortable celled fixed>
      <Table.Header>
        <Table.Row>
        <Table.HeaderCell
            sorted={column === 'name' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'runN' })}
            width = {1}
          >
            Run Number
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === 'age' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'key' })}
          >
            Keyword
          </Table.HeaderCell>
          {/* <Table.HeaderCell
            sorted={column === 'gender' ? direction : null}
            onClick={() => store.dispatch({ type: 'CHANGE_SORT', column: 'gender' })}
          >
            Gender
          </Table.HeaderCell> */}
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {newData.map(({ runN, key }) => (
          <Table.Row key={runN}>
            <Table.Cell>{runN}</Table.Cell>
            <Table.Cell>{key}</Table.Cell>
            {/* <Table.Cell>{gender}</Table.Cell> */}
          </Table.Row>
        ))}
      </Table.Body>
    </Table>
  )
}


function ImportData() {
  let fileReader;

  const handleFileRead = (e) => {
      const content = fileReader.result;
      const content2 = JSON.parse(content);
      // console.log(content2)
      store.dispatch({
        type: 'ADD_MESSAGE',
        state: content2,
      });
  };
  
  const handleFileChosen = (file) => {
    fileReader = new FileReader();
    fileReader.onloadend = handleFileRead;
    fileReader.readAsText(file);
  };

  return (
    <div>
      <input
        type='file'
        id='file'
        className='input-file'
        accept='.json'
        onChange={e => handleFileChosen(e.target.files[0])}
      />
    </div>
  );
}

class FilterInput extends React.Component {
  state = {
    value: '',
  };

  onChange = (e) => {
    this.setState({
      value: e.target.value,
    })
    store.dispatch({
      type: 'Filter_MESSAGE',
      text: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <div className='ui input'>
          <input
            onChange={this.onChange}
            value={this.state.value}
            type='text'
            placeholder= "Search"
          />
        </div>
      </div>
    );
  }
}


class App extends React.Component {
  componentDidMount() {
    store.subscribe(() => this.forceUpdate());
  }

  render() {
    const state = store.getState();
    console.log("A")
    console.log(state)
    return(
      <div className = 'ui segment'>
        <ImportData />
        <FilterInput />
        <TableExampleSortable tableData = {state} />
      </div>
    );
  }

}


export default App;
